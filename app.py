import os
from flask import Flask, request, make_response, render_template
from softcore.model import Interpreter
import time
from bs4 import BeautifulSoup
from flask_cors import CORS
import spacy
import re

# os.system("python -m spacy download pt")

model_class_text = Interpreter.load("./modelos/default/model_20201028-111131")
# model_ner_alienacao = Interpreter.load("./modelos/default/model_20200712-123310")

model_ner_alienacao = spacy.load("./modelos/model-ner")

model_contrato_alienacao_fiduciaria = spacy.load("./modelos/model-ner-2")

app = Flask(__name__)
CORS(app)

dict_from_to = {
    "RENAVAM_VEICULO": "renavam",
    "TIPO_VEICULO": "tipo",
    "MARCA_VEICULO": "marca",
    "MODELO_VEICULO": "modelo",
    "ANO_MODELO_VEICULO": "ano",
    "CHASSI_VEICULO": "chassi",
    "PLACA_VEICULO": "placa",
    "COR_VEICULO": "cor"
}


def clear(text):
    text = re.sub(r"_+", "", text)
    return " ".join(text.split())


@app.route('/')
def upload_file():
    return render_template('upload.html')


@app.route("/parse", methods=["GET", "POST"])
def parse():
    if request.method == "GET":
        return {"status": 200}
    if request.method == "POST":

        data = request.files["file"].read()
        soup = BeautifulSoup(data, "lxml")
        result = {"bem": {}}
        for p in soup("p"):
            res_parser = model_class_text.parse(p.text)

            if (
                res_parser["label"]["name"]
                not in [
                    "outros",
                ]
            ) and (res_parser["label"]["confidence"] >= 0.80):
                if result.get(res_parser["label"]["name"]) is None:
                    result[res_parser["label"]["name"]] = {
                        "text": res_parser["text"],
                        "confidence": res_parser["label"]["confidence"],
                    }

                    # CASO FOR ALIENAÇÃO FINDUCIARIA
                    # if res_parser["label"]["name"] == "alienacao_fiduciaria":
                    #     res_ner = model_ner_alienacao.parse(str(p.text).replace("\n", " "))
                    #     for ent in res_ner["entities"]:
                    #         if ent["confidence"] >= 0.60:
                    #             result["bem"][dict_from_to[ent["entity"]]] = ent["value"]
                    if res_parser["label"]["name"] == "alienacao_fiduciaria":
                        res_ner = model_ner_alienacao(
                            clear(str(p.text).replace("\n", " ")))
                        for ent in res_ner.ents:
                            result["bem"][dict_from_to[ent.label_]] = ent.text
                    if res_parser["label"]["name"] == "contrato_alienacao_fiduciaria":
                        res_ner = model_contrato_alienacao_fiduciaria(
                            clear(str(p.text).replace("\n", " ")))
                        for ent in res_ner.ents:
                            result["bem"][dict_from_to[ent.label_]] = ent.text

        return make_response(result, 200)


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port, debug=True)
