# Propositor de Sentenças de Busca e Apreensão - Backend

Este é o backend para a POC de classificação de petições iniciais e proposição de documentos de sentença para casos de Busca e Apreensão.

## Instruções

1. Baixar o projeto
2. `pip install -r requirements.txt`
3. `python -m spacy download pt`
4. `python app.py`

O projeto vai inciar na porta `5000`, é possivel definir a variável de ambiente `PORT` para alterar a porta em que o serviço inicia.

## Endpoints

### /

Retorna uma pagina com um formulário de upload. Deve ser enviado um arquivo HTML com os blocos de informações delimitados por `<p></p>`.

### /parse

Recebe um arquivo HTML e retorna um JSON. Um exemplo de envio pode ser visto no endpoint acima.

