FROM python:3.7-slim-buster

RUN apt-get update && apt-get install -y  build-essential && apt-get install -y git manpages-dev

COPY requirements.txt .
RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

RUN python -m spacy download pt

EXPOSE 5000

CMD ["python", "app.py"]